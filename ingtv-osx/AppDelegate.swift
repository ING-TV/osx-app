//
//  AppDelegate.swift
//  ingtv-osx
//
//  Created by Alexander on 19.11.15.
//  Copyright © 2015 ING-DiBa. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(aNotification: NSNotification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(aNotification: NSNotification) {
        // Insert code here to tear down your application
    }


}

