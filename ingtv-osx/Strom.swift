//
//  Strom.swift
//  ingtv-osx
//
//  Created by Alexander on 21.11.15.
//  Copyright © 2015 ING-DiBa. All rights reserved.
//

import Cocoa
import AVFoundation
//import CoreGraphics



class Strom : NSObject {
    
    let session: AVCaptureSession
    let output: AVCaptureVideoDataOutput
    
    let queue = dispatch_queue_create("image-queue", DISPATCH_QUEUE_SERIAL)
    
    

    override init() {
        let device = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo)
        let deviceInput = try! AVCaptureDeviceInput(device: device)
        
        session = AVCaptureSession()
        output = AVCaptureVideoDataOutput()
        
        
        super.init()
        
        
        session.sessionPreset = AVCaptureSessionPresetMedium
        session.addInput(deviceInput)
        
        output.videoSettings = [
            kCVPixelBufferPixelFormatTypeKey as NSString : NSNumber(unsignedInt: kCVPixelFormatType_32BGRA)
        ]
//        output.minFrameDuration = CMTimeMake(1, 15);
        output.setSampleBufferDelegate(self, queue: queue)
        
        session.addOutput(output)
        
        
        
        
        session.startRunning()
    }

    
}

extension Strom : AVCaptureVideoDataOutputSampleBufferDelegate {
    
    func captureOutput(captureOutput: AVCaptureOutput!, didOutputSampleBuffer sampleBuffer: CMSampleBuffer!, fromConnection connection: AVCaptureConnection!) {
        let image = imageFromSampleBuffer(sampleBuffer)
        
        let path = ("~/Sites" as NSString).stringByExpandingTildeInPath
        let url = NSURL(fileURLWithPath: path, isDirectory: true)
        
        let imageUrl = url.URLByAppendingPathComponent("image.jpg")
        
        
        let tiffData = image.TIFFRepresentation!
        let imageRep = NSBitmapImageRep(data:tiffData)!
        let imageProperties = [ NSImageCompressionFactor : NSNumber(float: 0.1) ]
        let jpegData = imageRep.representationUsingType(.NSJPEGFileType, properties:imageProperties)!
        jpegData.writeToURL(imageUrl, atomically: true)
        
    }
    
    func captureOutput(captureOutput: AVCaptureOutput!, didDropSampleBuffer sampleBuffer: CMSampleBuffer!, fromConnection connection: AVCaptureConnection!) {
        print("did drop")
    }
}

// Create a UIImage from sample buffer data
func imageFromSampleBuffer(sampleBuffer: CMSampleBuffer) -> NSImage {
    // Get a CMSampleBuffer's Core Video image buffer for the media data
    let imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer)!
    // Lock the base address of the pixel buffer
    CVPixelBufferLockBaseAddress(imageBuffer, 0)
    
    // Get the number of bytes per row for the pixel buffer
    let baseAddress: UnsafeMutablePointer<Void> = CVPixelBufferGetBaseAddress(imageBuffer)
    
    // Get the number of bytes per row for the pixel buffer
    let bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer)
    // Get the pixel buffer width and height
    let width = CVPixelBufferGetWidth(imageBuffer)
    let height = CVPixelBufferGetHeight(imageBuffer)
    
    // Create a device-dependent RGB color space
    let colorSpace = CGColorSpaceCreateDeviceRGB()!
    
    // Create a bitmap graphics context with the sample buffer data
    let bitmapInfo: UInt32 = CGBitmapInfo.ByteOrder32Little.rawValue | CGImageAlphaInfo.PremultipliedFirst.rawValue
    let context = CGBitmapContextCreate(baseAddress, width, height, 8,
        bytesPerRow, colorSpace, bitmapInfo)
    // Create a Quartz image from the pixel data in the bitmap graphics context
    let quartzImage = CGBitmapContextCreateImage(context)!
    // Unlock the pixel buffer
    CVPixelBufferUnlockBaseAddress(imageBuffer,0)
    
    // Create an image object from the Quartz image
    let image = NSImage(CGImage: quartzImage, size: NSSize(width: width, height: height))
    
    return (image);
}
