//
//  ViewController.swift
//  ingtv-osx
//
//  Created by Alexander on 19.11.15.
//  Copyright © 2015 ING-DiBa. All rights reserved.
//

import Cocoa
import AVFoundation

class ViewController: NSViewController {
    
    @IBOutlet weak var previewView: NSView!
    
    
    let strom = Strom()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let previewLayer = AVCaptureVideoPreviewLayer(session: strom.session)
        previewView.layer = previewLayer
    }

    override var representedObject: AnyObject? {
        didSet {
        // Update the view, if already loaded.
        }
    }
    
    
}

